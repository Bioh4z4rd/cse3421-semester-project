USE [master]
GO

alter database mtg_card_database set single_user with rollback immediate

IF EXISTS (SELECT * FROM sys.sysdatabases WHERE NAME = 'mtg_card_database')
BEGIN 
   DROP DATABASE mtg_card_database 
END

CREATE DATABASE mtg_card_database
GO

USE mtg_card_database
GO

CREATE TABLE BLOCK
	(name			VARCHAR(200)		NOT NULL,
	 startdate		DATE				NOT NULL,
	 enddate		DATE						,
	 _description	TEXT						,

	 PRIMARY KEY(name)
	);

CREATE TABLE SET_
	(name			VARCHAR(200)		NOT NULL,
	 release_date	DATE				NOT NULL,
	 symbol			VARCHAR(100)		NOT NULL,
	 base			BIT							,
	 expansion		BIT							,
	 block			VARCHAR(200)				,

	 PRIMARY KEY(name)							,
	 FOREIGN KEY(block) REFERENCES BLOCK(name)
	);

CREATE TABLE RULING
	(id				INT					NOT NULL,
	 _date			DATE				NOT NULL,
	 _text			TEXT				NOT NULL

	 PRIMARY KEY(id)
	);

CREATE TABLE CARD_
	(name			VARCHAR(500)		NOT NULL,
	 multiverseID	INT					NOT NULL,
	 _type		VARCHAR(100)			NOT NULL,
	 cmc			INT					NOT NULL,
	 rules_text		VARCHAR(1000)				,
	 flavor_text	VARCHAR(1000)				,		
	 p_t			VARCHAR(7)					,
	 
	 artist			VARCHAR(200)				,
	 _set			VARCHAR(200)				,
	 
	 ruling			INT							,

	PRIMARY KEY(multiverseID)					,
	FOREIGN KEY(_set) REFERENCES BLOCK(name)	,
	FOREIGN KEY(ruling) REFERENCES RULING(id)	
	);

CREATE TABLE RULING_OF
	(rulingID		INT					NOT NULL,
	 cardID			INT					NOT NULL,

	 PRIMARY KEY(rulingID, cardID),
	 FOREIGN KEY(rulingID) REFERENCES RULING(id),
	 FOREIGN KEY(cardID) REFERENCES CARD_(multiverseID)
	);
	
CREATE TABLE COPY
	(id				INT					NOT NULL,
	 multiverseID	INT					NOT NULL,
	 foil			BIT							,		
	 condition		VARCHAR(20)					,
	 lang			VARCHAR(50)					,
	
	PRIMARY KEY(id)								,
	FOREIGN KEY(multiverseID) REFERENCES CARD_(multiverseID)
	);
GO

CREATE PROCEDURE get_last_card
	@max_id INT OUTPUT
AS
	SELECT MAX(multiverseID)
	FROM CARD_;
GO

-- create the user for the card-injector.

CREATE USER cardsniffer FOR LOGIN cardsniffer

EXEC sp_addrolemember 'db_datawriter', 'cardsniffer';
EXEC sp_addrolemember 'db_datareader', 'cardsniffer';



/*
-----------------------------------------------
Create the user-side.

USE [master]
GO

alter database user_database set single_user with rollback immediate

IF EXISTS (SELECT * FROM sys.sysdatabases WHERE NAME = 'user_database')
BEGIN 
   DROP DATABASE user_database 
END

CREATE DATABASE user_database
GO

USE user_database
GO

CREATE TABLE _PROFILE
	(accountID	VARCHAR(20)			,
	 fname		VARCHAR(20)			,
	 lname		VARCHAR(30)			, 
	 city		VARCHAR(30)			,
	 _state		VARCHAR(20)			,
	 country	VARCHAR(30)			,


	);

*/