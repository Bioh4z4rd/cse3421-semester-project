from bs4 import *
from html.parser import *
from urllib import request
import re
import argparse
from os import getenv
import _mssql

parser = argparse.ArgumentParser() #yay....a comment
parser.add_argument('-s', type=int, default=1)
parser.add_argument('end', type=int)
parser.add_argument('--verbose', '-v',  action='count', default=0)
args = parser.parse_args()
# ----------------------------------------

def getCardPage(id):
	page = request.urlopen("http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=" + str(id)).read()
	page = page.decode('utf-8')
	pattern = re.compile(r'[\t\n]+')
	page = re.sub(pattern, '', page)

	return BeautifulSoup(page)

# ----------------------------------------

def getCardName(card_soup):
	name_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_nameRow")))
	
	try:
		name = name_soup.find(class_="value").string
		name = name.strip()
	except AttributeError:
		name = "NULL"

	return name

def getCardCMC(card_soup):
	cmc_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_cmcRow")))
	try:
		cmc = cmc_soup.find(class_="value").text
		cmc = int(cmc.strip())
	except AttributeError:
		cmc = None

	return cmc

def getCardType(card_soup):
	type_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_typeRow")))
	try:
		_type = type_soup.find(class_="value").string
		_type = _type.strip()
		_type = _type.replace(u'\u2014','-')
	except AttributeError:
		_type = "NULL"

	return _type
	

def getCardRulesText(card_soup):
	rules_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_textRow")))
	try:
		rules = rules_soup.find(class_="value").text
		rules = rules.strip()
	except AttributeError:
		rules = "NULL"

	return rules

def getCardFlavorText(card_soup):
	flavor_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_flavorRow")))
	try:
		flavor = flavor_soup.find(class_="value").text
		flavor = flavor.strip()
	except AttributeError:
		flavor = "NULL"

	return flavor

def getCardManaCost(card_soup):
	pass

# Exracts the power and toughness of a card, returning them in
# an array of size 2
def getP_T(card_soup):
	p_t_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_ptRow")))
	try:
		p_t = p_t_soup.find(class_="value").text
		p_t = p_t.replace(u'\u2014','').split('/')
		p_t = p_t.replace(r' ','').split('/')
		for p, i in enumerate(p_t):
			p_t[p] = int(i)
	except AttributeError:
		p_t = 'NULL'

	return p_t



# ----------------------------------------

# Gets a single card's info from the source page soup and extracts its info into an array in the
# following format for a _CARD tuple in the mtg database:
# 
def getCardInfo(card_soup):
	info = []
	
	info.append(getCardName(card_soup))
	info.append(getCardCMC(card_soup))
	info.append(getCardType(card_soup))
	info.append(getCardRulesText(card_soup))
	info.append(getCardFlavorText(card_soup))
	info.append(getP_T(card_soup))

	return info

# ----------------------------------------

def processRegularCard(card_soup):
	pass

# Processes a double-sided card's info into two arrays.
def processDoubleCard(double_card_soup):
	pass

# ----------------------------------------

def connectToDB():
	return _mssql.connect(server='JUSTIN-PC', user='cardsniffer', 
		password='213r8kz4m98ds91', database='mtg_card_database');

def injectCardInfoIntoDB(id, card_info, conn):
	name = cardInfo[0].replace('\'', '\'\'').replace('\"', '\"\"')
	rules = cardInfo[3].replace('\'', '\'\'').replace('\"', '\"\"')
	flavor = cardInfo[4].replace('\'', '\'\'').replace('\"', '\"\"')
	conn.execute_query("INSERT INTO CARD_(name, multiverseID, _type, cmc, rules_text, flavor_text) VALUES(\'%s\', %i, \'%s\', %i, \'%s\', \'%s\')" %(name, id,
		cardInfo[2], cardInfo[1], rules, flavor))

def injectDoubleCardSQL(cards_info):
	pass

# ----------------------------------------
# main
# ----------------------------------------

conn = connectToDB();

for id in range(args.s,args.end + 1):

	card_soup = getCardPage(id)

	cardInfo = getCardInfo(card_soup)

	print("\nmultiverseid=%s\n\tName: %s" %(id, cardInfo[0]))
	if args.verbose >= 1:
		print("\tCMC:  %s\n\tType: %s\n\tRules Text: %s\n\tFlavor Text: %s\n" %(cardInfo[1], cardInfo[2], cardInfo[3], cardInfo[4]))
		if len(cardInfo) > 5 and cardInfo[5] is not None:
			print("\tP/T: %i/%i" % (cardInfo[5][0], cardInfo[5][1]))

	injectCardInfoIntoDB(id, cardInfo, conn)

print("All cards loaded.")