USE mtg_card_database
GO

CREATE VIEW no_duplicate_named_cards AS
	SELECT A.name
	FROM CARD_ as A, CARD_ AS B
	WHERE A.name=B.name;